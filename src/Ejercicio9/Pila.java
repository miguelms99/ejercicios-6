package Ejercicio9;

import java.util.ArrayList;
import java.util.NoSuchElementException;

/**
 * @author Miguel Moreno Such
 */
public class Pila<T> {

    ArrayList<T> pila;

    public Pila() {
        pila = new ArrayList<T>();
    }

    public void push(T elemento) {
        pila.add(elemento);
    }

    public T pop() {
        if (pila.isEmpty()) throw new NoSuchElementException();
        return pila.remove(pila.size() - 1);
    }

}
