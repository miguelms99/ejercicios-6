package Ejercicio9;

/**
 * @author Miguel Moreno Such
 */
public class Ejercicio9 {

    public static void main(String[] args) {

        //Pruebo que funciona
        Pila<String> pila = new Pila<String>();
        for (int i = 0; i <= 30; i++) {
            pila.push(String.valueOf(i));
        }
        for (int i = 0; i <= 30; i++) {
            System.out.println(pila.pop());
        }

    }

}