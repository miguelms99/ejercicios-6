package Ejercicio5;

import java.util.ArrayList;

public class Ejercicio5 {

    public static void main(String[] args) {
        //Pruebo los métodos
        ArrayList<String> lista1 = new ArrayList<>();
        ArrayList<String> lista2 = new ArrayList<>();
        lista1.add("a");
        lista1.add("b");
        lista1.add("c");
        lista1.add("d");
        lista2.add("c");
        lista2.add("d");
        lista2.add("e");
        lista2.add("f");
        ArrayList<String> union = union(lista1, lista2);
        ArrayList<String> interseccion = interseccion(lista1, lista2);
        System.out.println("Unión");
        for (String s : union) {
            System.out.println(s);
        }
        System.out.println("Intersección");
        for (String s : interseccion) {
            System.out.println(s);
        }
    }

    private static <T> ArrayList<T> union(ArrayList<T> lista1, ArrayList<T> lista2) {
        ArrayList<T> listaUnion = new ArrayList<T>(lista1);
        listaUnion.removeAll(lista2);
        listaUnion.addAll(lista2);
        return listaUnion;
    }

    private static <T> ArrayList<T> interseccion(ArrayList<T> lista1, ArrayList<T> lista2) {
        ArrayList<T> listaUnion = new ArrayList<T>(lista1);
        listaUnion.retainAll(lista2);
        return listaUnion;
    }

}
