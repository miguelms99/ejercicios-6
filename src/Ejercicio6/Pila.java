package Ejercicio6;

import java.util.Arrays;
import java.util.NoSuchElementException;

/**
 * @author Miguel Moreno Such
 */
public class Pila<T> {

    private int capacidad;
    private Object[] pila;
    private int tamano;

    public Pila() {
        capacidad = 8; //Capacidad inicial
        tamano = 0;
        pila = new Object[capacidad];
    }

    public void push(T elemento) {
        if (tamano == capacidad) {
            capacidad *= 2;
            pila = Arrays.copyOf(pila, capacidad);
        }
        pila[tamano++] = elemento;
    }

    @SuppressWarnings("unchecked")
    public T pop() {
        if (tamano == 0) throw new NoSuchElementException();
        return (T) pila[--tamano];
    }

}
