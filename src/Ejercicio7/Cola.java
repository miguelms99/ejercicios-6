package Ejercicio7;

import java.util.Arrays;
import java.util.NoSuchElementException;

/**
 * @author Miguel Moreno Such
 */
public class Cola<T> {

    private int capacidad;
    private Object[] cola;
    private int head;
    private int tail;

    public Cola() {
        capacidad = 16; //Capacidad inicial
        tail = 0;
        head = 0;
        cola = new Object[capacidad];
    }

    public void enqueue(T elemento) {
        if (tail == capacidad) {
            //Si la cola tiene tamaño < capacidad/2 solo se reordena el contenido
            //Si tiene tamaño >= capacidad/2 también se amplía la capacidad
            if (tail-head >= capacidad/2) capacidad *= 2;
            cola = Arrays.copyOfRange(cola, head, head+capacidad);
            tail -= head;
            head = 0;
        }
        cola[tail++] = elemento;
    }

    @SuppressWarnings("unchecked")
    public T dequeue() {
        if (head == tail) throw new NoSuchElementException();
        return (T) cola[head++];
    }

}
