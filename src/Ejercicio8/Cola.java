package Ejercicio8;

import java.util.ArrayList;
import java.util.NoSuchElementException;

/**
 * @author Miguel Moreno Such
 */
public class Cola<T> {

    private ArrayList<T> cola;

    public Cola() {
        cola = new ArrayList<T>();
    }

    public void enqueue(T elemento) {
        cola.add(elemento);
    }

    public T dequeue() {
        if (cola.isEmpty()) throw new NoSuchElementException();
        return cola.remove(0);
    }

}
