package Ejercicio8;

/**
 * @author Miguel Moreno Such
 */
public class Ejercicio8 {

    public static void main(String[] args) {

        //Pruebo que funciona
        Cola<String> cola = new Cola<String>();
        for (int i = 0; i <= 30; i++) {
            cola.enqueue(String.valueOf(i));
        }
        for (int i = 0; i <= 30; i++) {
            System.out.println(cola.dequeue());
        }

    }

}
